/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

interface JQuery {
  hoverfold(options?: any): any;
}
