import { Component, OnInit } from '@angular/core';
import { Coordinates } from '../_models/coordinates';

@Component({
  selector: 'app-coordinates',
  templateUrl: './coordinates.component.html',
  styleUrls: ['./coordinates.component.scss']
})
export class CoordinatesComponent implements OnInit {

  coordinates: Array<Coordinates> = new Array();
  currentLocation: any;
  destinationLocation: any;
  googleMapsLink: string;
  currentSelection: number;

  constructor() {
    this.coordinates.push(new Coordinates(39.7816986, -8.878637, '../../assets/images/groom-s.png', 'Noivo',
     'Pequeno-almoço do Noivo', '9h30m', 'Pequeno-almoço', 'Grupo Desportivo de Casal Novo,\nRua da Escola, \nCasal Novo 2400-766 Amor'));
    this.coordinates.push(new Coordinates(39.7990597, -8.9271887, '../../assets/images/bride-s.png', 'Noiva',
     'Casa da Noiva', '9h30m', 'Pequeno-almoço', 'Rua cova do Lobo, \nTravessa da Cancela nº 5, \n2430-131 Escoura'));
    this.coordinates.push(new Coordinates(39.802922, -8.860281, '../../assets/images/church-s.png', 'Igreja',
     'Igreja d\'Amor', '11h00m', 'Cerimónia', 'Largo Padre Margalhau, nº 1 \n2400-788, Amor'));
    this.coordinates.push(new Coordinates(39.519509, -8.772633, '../../assets/images/bride-groom-s.png', 'Quinta',
     'Quinta d\' Aldeia', '14h00m', 'Festa', 'Barreira – Junqueira \nSão Bento Porto de Mós, \n2480-133 Porto de Mós'));
  }

  ngOnInit() {
      this.currentSelection = 0;

      if (navigator){
        navigator.geolocation.getCurrentPosition(pos => {
          this.currentLocation = { lat: pos.coords.latitude, lng: pos.coords.longitude };
          this.destinationLocation = { lat: this.coordinates[this.currentSelection].lat, lng: this.coordinates[this.currentSelection].lng };
          this.googleMapsLink = `http://maps.google.com/?saddr=${+pos.coords.latitude},${+pos.coords.longitude}&daddr=${this.coordinates[this.currentSelection].lat},${this.coordinates[this.currentSelection].lng}`;
        });
      }
  }

  setLocation(btnNum){
    this.currentSelection = btnNum;
    this.destinationLocation = { lat: this.coordinates[this.currentSelection].lat, lng: this.coordinates[this.currentSelection].lng };
  }

  getDestination() {

    return `http://maps.google.com?daddr=${this.coordinates[this.currentSelection].lat},${this.coordinates[this.currentSelection].lng}`;
  }
}
