import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { WeddingGiftsComponent } from './wedding-gifts/wedding-gifts.component';
import { CoordinatesComponent } from './coordinates/coordinates.component';
import { WeddingContributionsComponent } from './wedding-contributions/wedding-contributions.component';

const appRoutes: Routes = [
    { path: '', component: LandingPageComponent },
    { path: 'wedding-gifts', component: WeddingGiftsComponent },
    { path: 'wedding-coordinates', component: CoordinatesComponent },
    { path: 'wedding-contributions', component: WeddingContributionsComponent }
  ];

  export const appRoutingProviders: any[] = [];

  export const routing = RouterModule.forRoot(appRoutes);
