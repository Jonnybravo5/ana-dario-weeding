
import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import 'rxjs/add/operator/map';

import { ModalService } from '../_services';
import { ContributionService } from '../_services/constribution.service';

import { Gift } from '../_models/gift';
import { resetFakeAsyncZone } from '@angular/core/testing';

import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'gift-modal',
  templateUrl: './gift-modal.component.html',
  styleUrls: ['./gift-modal.component.scss']
})
export class GiftModalComponent implements OnInit {

  giftModalId = 'gift-modal';

  email: string;
  name: string;
  value: number;

  success: boolean;
  msg: string;

  @Input() isModalOpen: boolean;
  @Input() gift: Gift;

  constructor(private modalService: ModalService, private afs: AngularFirestore, private contributionService: ContributionService, private sanitizer: DomSanitizer) {
    modalService.showModal.subscribe(
      (showModal) => {
        showModal ? this.openModal() : this.closeModal();
      }
    );

    modalService.contributionSuccessful.subscribe(
      (cs) => {
        this.success = cs;
        this.msg="Obrigado pela contribuição!";
      }
    );

  }

  ngOnInit() {
  }

  openModal() {
    this.reset();
    this.modalService.open(this.giftModalId);
  }

  closeModal() {
    this.modalService.close(this.giftModalId);
  }

  addContribution() {
    if (this.email && this.name && this.value) {
      //Get the input value
      let contribution = {
        name: this.name,
        email: this.email,
        value: this.value,
        approved: false,
        gift: this.gift.id
      }

      this.contributionService.addContribution(contribution);
    }else{
      this.msg="nao da"
    }
  }

  getBackgroundImage(image) {
    return {'background-image': 'linear-gradient(rgba(255,255,255,0.7), rgba(255,255,255,0.7)), url(' + image + ')'}
  }

  private reset() {
    this.success = false;
    this.msg = "";

    this.email = "";
    this.name = "";
    this.value = null;
  }
}
