import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  @Input() ribbonImage: any;

  isMenuOpen: boolean;

  constructor(private router: Router, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.ribbonImage = this.ribbonImage ? this.sanitizer.bypassSecurityTrustStyle(`url('./../../assets/images/${this.ribbonImage}.jpeg')`) : null;
  }

  goToHome() {
    this.router.navigate(['/']);
  }

  goToGifts() {
    this.router.navigate(['/wedding-gifts']);
  }

  goToLocations() {
    this.router.navigate(['/wedding-coordinates']);
  }

  openMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  verifyLocation(path) {
    return location.pathname.includes(path);
  }

}
