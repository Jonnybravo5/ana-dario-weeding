import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as ProgressBar from 'progressbar.js/dist/progressbar';

import { ContributionService } from '../_services/constribution.service';
import { config } from "./../app.config";
import { Contribution } from '../_models/contribution';
import { Gift } from '../_models/gift';

@Component({
  selector: 'app-wedding-contributions',
  templateUrl: './wedding-contributions.component.html',
  styleUrls: ['./wedding-contributions.component.scss']
})
export class WeddingContributionsComponent implements OnInit {

  contributions: Observable<any[]>;

  constructor(private db: AngularFirestore, private contributionService: ContributionService) {
  }

  ngOnInit() {
    this.contributions = this.db
      .collection(config.contributions_collection_endpoint)
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          //Get document data
          const data = a.payload.doc.data() as Contribution;
          //Get document id
          const id = a.payload.doc.id;
          //Use spread operator to add the id to the document data
          return { id, ...data};
        });
      });


    /*this.contributions = this.db
      .collection(config.contributions_collection_endpoint)
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          //Get document data
          const data = a.payload.doc.data() as Contribution;
          const giftId = data.gift;
          //Get document id
          const id = a.payload.doc.id;

          return this.db.collection(config.gifts_collection_endpoint)
            .doc(giftId)
            .snapshotChanges()
            .map(b => {
              const gift = b.payload.data() as Gift;

              return { id, ...data, gift };
            });
        });
      });*/
  }

  approveContribution(contribution) {
    contribution.approved = true;
    //1 actualizar contribuicao
    this.contributionService.updateContribution(contribution.id, contribution);
  }
}
