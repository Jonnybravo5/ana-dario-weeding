import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeddingContributionsComponent } from './wedding-contributions.component';

describe('WeddingContributionsComponent', () => {
  let component: WeddingContributionsComponent;
  let fixture: ComponentFixture<WeddingContributionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeddingContributionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeddingContributionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
