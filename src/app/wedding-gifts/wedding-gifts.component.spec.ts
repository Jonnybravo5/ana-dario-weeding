import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeddingGiftsComponent } from './wedding-gifts.component';

describe('WeddingGiftsComponent', () => {
  let component: WeddingGiftsComponent;
  let fixture: ComponentFixture<WeddingGiftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeddingGiftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeddingGiftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
