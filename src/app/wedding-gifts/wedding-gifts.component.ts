import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import * as ProgressBar from 'progressbar.js/dist/progressbar';

import { ModalService } from '../_services';
import { GiftService } from '../_services/gift.service';
import { config } from "./../app.config";
import { Gift } from '../_models/gift';


@Component({
  selector: 'app-wedding-gifts',
  templateUrl: './wedding-gifts.component.html',
  styleUrls: ['./wedding-gifts.component.scss']
})


export class WeddingGiftsComponent implements OnInit {
  gifts: Observable<any[]>;


  isModalOpen: boolean;
  data: Gift;

  @ViewChildren('gifts') giftContentView: QueryList<Gift>;

  constructor(private db: AngularFirestore, private giftService: GiftService, private modalService: ModalService) {}

  ngOnInit() {
    this.gifts = this.db
      .collection(config.gifts_collection_endpoint)
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          //Get document data
          const data = a.payload.doc.data() as Gift;
          //Get document id
          const id = a.payload.doc.id;
          let percentage = Math.round((data.acc/data.price)*100);
          //Use spread operator to add the id to the document data
          return { id, ...data, percentage};
        });
      });
  }

  ngAfterViewInit() {
    this.giftContentView.changes.subscribe(g => {
      for(let i = 0; i < g.length; i++) {
        let line = new ProgressBar.Path('#heart-path-'+i, {
          easing: 'easeInOut',
          duration: 1400
        });

        let elemAttributes = g._results[i].nativeElement.attributes;
        let percentage = elemAttributes['data-percentage'].value > 100 ? 100 : elemAttributes['data-percentage'].value;

        line.set(0);
        line.animate(percentage/100);
      }
    })
  }

  openGiftModal(gift) {
    if(gift.percentage > 100)
      return;

    this.isModalOpen = true;
    this.data = gift;
    this.modalService.showModal.emit(this.isModalOpen);
  }
}
