import { config } from "./../app.config";
import { Gift } from "./../_models/gift";
import { Injectable } from "@angular/core";

import {
  AngularFirestoreDocument,
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";

@Injectable()
export class GiftService {
  Gifts: AngularFirestoreCollection<Gift>;
  private GiftDoc: AngularFirestoreDocument<Gift>;

  constructor(private db: AngularFirestore) {
    //Get the Gifts collecction
    this.Gifts = db.collection<Gift>(config.gifts_collection_endpoint);
  }

  addGift(gift) {
    //Add the new Gift to the collection
    this.Gifts.add(gift);
  } //addGift

  updateGift(id, update) {
    //Get the Gift document
    this.GiftDoc = this.db.doc<Gift>(`${config.gifts_collection_endpoint}/${id}`);

    this.GiftDoc.update(update);
  } //updateGift

  deleteGift(id) {
    //Get the Gift document
    this.GiftDoc = this.db.doc<Gift>(`${config.gifts_collection_endpoint}/${id}`);

    //Delete the document
    this.GiftDoc.delete();
  } //deleteGift
}