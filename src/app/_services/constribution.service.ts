import { config } from "../app.config";
import { Contribution } from "../_models/contribution";
import { Injectable } from "@angular/core";

import { ModalService } from './modal.service';

import {
  AngularFirestoreDocument,
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";

@Injectable()
export class ContributionService {
  Contributions: AngularFirestoreCollection<Contribution>;
  private ContributionDoc: AngularFirestoreDocument<Contribution>;

  constructor(private db: AngularFirestore, private modalService: ModalService) {
    //Get the Contributions collecction
    this.Contributions = db.collection<Contribution>(config.contributions_collection_endpoint);
  }

  addContribution(contribution) {
    let _this = this;

    //Add the new Contribution to the collection
    this.Contributions.add(contribution).then(function () {
      _this.modalService.contributionSuccessful.emit(true);
    }).catch(function (error) {
      _this.modalService.contributionSuccessful.emit(false);
      });
  }

  updateContribution(id, update) {
    //Get the Contribution document
    this.ContributionDoc = this.db.doc<Contribution>(`${config.contributions_collection_endpoint}/${id}`);

    this.ContributionDoc.update(update);
  } //updateContribution

  deleteContribution(id) {
    //Get the Contribution document
    this.ContributionDoc = this.db.doc<Contribution>(`${config.contributions_collection_endpoint}/${id}`);

    //Delete the document
    this.ContributionDoc.delete();
  } //deleteContribution
}