import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'

import { AppComponent } from './app.component';
import { SendEmailComponent } from './send-email/send-email.component';
import { WeddingGiftsComponent } from './wedding-gifts/wedding-gifts.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { CoordinatesComponent } from './coordinates/coordinates.component';
import { WeddingContributionsComponent } from './wedding-contributions/wedding-contributions.component';
import { GiftModalComponent } from './gift-modal/gift-modal.component';

import { ModalComponent } from './_directives';
import { ModalService } from './_services';
import { FormsModule } from '@angular/forms';
import { GiftService } from './_services/gift.service';
import { ContributionService } from './_services/constribution.service';
import { TopMenuComponent } from './top-menu/top-menu.component';

var firebaseConfig  = {
  apiKey: "AIzaSyD25mMj7c3tRsLpC3CKF5XO0rZKM6EvvbA",
  authDomain: "ana-e-dario.firebaseapp.com",
  databaseURL: "https://ana-e-dario.firebaseio.com",
  projectId: "ana-e-dario",
  storageBucket: "ana-e-dario.appspot.com",
  messagingSenderId: "157011752114"
};

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    SendEmailComponent,
    WeddingGiftsComponent,
    LandingPageComponent,
    CoordinatesComponent,
    WeddingContributionsComponent,
    GiftModalComponent,
    TopMenuComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA7Bnme7TQMY7V0ECURU8D01qYTwqvVwUU'
    }),
    AgmDirectionModule,
    routing
  ],
  providers: [appRoutingProviders, ModalService, GiftService, ContributionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
