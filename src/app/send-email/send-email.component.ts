import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.scss']
})
export class SendEmailComponent implements OnInit {

  endpoint = 'https://your-project.cloudfunctions.net/httpEmail';

  constructor(private http: HttpClient) { }


  ngOnInit() {
  }

}
