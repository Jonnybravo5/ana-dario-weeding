export class Gift {
  public id : string;
  public name: string;
  public description: string;
  public price: number;
  public acc: number;
  public percentage: number;
}
