export class Coordinates {
  public lat: number;
  public lng: number;
  public url_icon: string;
  public zoom: number;
  public name: string;
  public place: string;
  public hour: string;
  public event: string;
  public address: string;

  constructor(lat, lng, icon, name, place, hour, event, address){
    this.lat = lat;
    this.lng = lng;
    this.url_icon = icon;
    this.zoom = 15;
    this.name = name;
    this.place = place;
    this.hour = hour;
    this.event = event;
    this.address = address;
  }
}
