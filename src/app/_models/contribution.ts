import { Gift } from "./gift";

export class Contribution {
  public id : string;
  public approved: boolean;
  public email: string;
  public gift: string;
  public name: string;
  public value: number;
  public g: Gift;
}