const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const nodemailer = require('nodemailer');
exports.approvedContribution = functions.firestore
    .document('contributions/{contributionsId}')
    .onUpdate((change, context) => {
    console.log("here");
    // ...or the previous value before this update
    const previousObj = change.before.data();
    // Get an object representing the current document
    const newObj = change.after.data();
    const value = newObj.value;
    const giftId = newObj.gift;
    const email = newObj.email;
    const name = newObj.name;
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'anaedario05out18@gmail.com',
            pass: 'Wedding05out18'
        }
    });
    const db = admin.firestore();
    return db.collection("gifts").doc(giftId)
        .get()
        .then(doc => {
        const gift = doc.data();
        gift.acc += value;
        db.collection("gifts").doc(giftId).update("acc", (gift.acc + value));
        const mailOptions = {
            from: 'Ana & Dário <anaedario05out18@gmail.com>',
            to: email,
            cc: "jonnypetro89@gmail.com",
            subject: 'Obrigada pela sua participação!',
            html: `<!doctype html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><title></title><!--[if !mso]><!-- --><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><style type="text/css">#outlook a { padding:0; }
          .ReadMsgBody { width:100%; }
          .ExternalClass { width:100%; }
          .ExternalClass * { line-height:100%; }
          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
          p { display:block;margin:13px 0; }</style><!--[if !mso]><!--><style type="text/css">@media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
          }</style><!--<![endif]--><!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]--><!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]--><!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"><style type="text/css">@import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);</style><!--<![endif]--><style type="text/css">@media only screen and (min-width:480px) {
        .mj-column-per-25 { width:25% !important; max-width: 25%; }
.mj-column-per-50 { width:50% !important; max-width: 50%; }
.mj-column-per-100 { width:100% !important; max-width: 100%; }
      }</style><style type="text/css"></style><style type="text/css">.op3 {opacity:0.3;} .op5 {opacity:0.5;} .op7 {opacity:0.7;} .op8 {opacity:0.8;} .op9 {opacity:0.9;}</style></head><body style="background-color:#d6dde5;"><div style="background-color:#d6dde5;"><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--><div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"><tbody><tr><td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20;text-align:center;vertical-align:top;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:150px;" ><![endif]--><div class="mj-column-per-25 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"></table></div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:300px;" ><![endif]--><div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"><tr><td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"><p><span style="color: #0d2950;"><span style="font-weight: bold;"><span style="font-size: 16px;">Agradecemos a sua contribuição!</span></span></span></p></div></td></tr></table></div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:150px;" ><![endif]--><div class="mj-column-per-25 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"></table></div><!--[if mso | IE]></td></tr></table><![endif]--></td></tr></tbody></table></div><!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0;font-size:0;mso-line-height-rule:exactly;"><v:image style="border:0;height:0px;mso-position-horizontal:center;position:absolute;top:0;width:0px;z-index:-3;" src="https://firebasestorage.googleapis.com/v0/b/ana-e-dario.appspot.com/o/AD_namoro-39-email.jpg?alt=media&token=b09f6c7c-c664-4c7e-a5e0-31977110c0cc" xmlns:v="urn:schemas-microsoft-com:vml" /><![endif]--><div style="margin:0 auto;max-width:600px;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"><tr style="vertical-align:top;"><td background="https://firebasestorage.googleapis.com/v0/b/ana-e-dario.appspot.com/o/AD_namoro-39-email.jpg?alt=media&token=b09f6c7c-c664-4c7e-a5e0-31977110c0cc" style="background:#ffffff url(https://firebasestorage.googleapis.com/v0/b/ana-e-dario.appspot.com/o/AD_namoro-39-email.jpg?alt=media&token=b09f6c7c-c664-4c7e-a5e0-31977110c0cc) no-repeat center center / cover;background-position:center center;background-repeat:no-repeat;padding:0px;vertical-align:top;" height="300"><!--[if mso | IE]><table border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600" ><tr><td style=""><![endif]--><div class="mj-hero-content" style="margin:0px auto;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;margin:0px;"><tr><td><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;margin:0px;"><tr><td style="font-size:0px;word-break:break-word;"><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"><tr><td align="center" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;"><div style="font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"><p><span style="font-size: 16px; color: rgb(255, 255, 255);">Ana & Dário</span></p><p><br></p><p><br></p><p><br></p><p><br></p><p><span style="font-size: 27px; padding: 0 30px;"><span style="font-weight: bold;"><span style="color: rgb(255, 255, 255);">${name}</span></span></span></p><p><span style="font-size: 23px; padding: 0 30px;"><span style="font-weight: bold;"><span style="color: rgb(255, 255, 255);">contribuiu com ${value}€</span></span></span></p><p><span style="font-weight: bold;"><span style="color: rgb(255, 255, 255);"><span style="font-size: 23px;">para ${gift.name}</span></span></span></p></div></td></tr></table></div></td></tr></table></td></tr></table></div><!--[if mso | IE]></td></tr></table><![endif]--></td></tr></table></div><!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--><div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"><tbody><tr><td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20;text-align:center;vertical-align:top;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]--><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"><table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"><tr><td align="center" vertical-align="middle" style="font-size:0px;padding:15px 30px;padding-top:70;padding-right:25px;padding-bottom:10px;padding-left:25px;word-break:break-word;"><table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"><tr><td align="center" bgcolor="#8d2e26" role="presentation" style="border:none;border-radius:3px;cursor:auto;padding:10px 25px;" valign="middle"><a href="https://anaedario05out18.com" style="background:#8d2e26;color:#FFFFFF;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:15px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;" target="_blank">Visite o nosso site!</a></td></tr></table></td></tr></table></div><!--[if mso | IE]></td></tr></table><![endif]--></td></tr></tbody></table></div><!--[if mso | IE]></td></tr></table><![endif]--></div></body></html>` // plain text body
        };
        transporter.sendMail(mailOptions, function (err, info) {
            if (err)
                console.log(err);
            else
                console.log(info);
        });
    })
        .then(() => console.log('done'))
        .catch(err => console.log(err));
});
//# sourceMappingURL=index.js.map